; (define zero (lambda (f) (lambda (x) x)))
; (define (add-1 n) (lambda (f) (lambda (x) (f ((n f) x)))))
;
; (define one (add-1 zero))
; (define one (lambda (f) (lambda (x) (f (((lambda (f) (lambda (x) x)) f) x)))))
; (define one (lambda (f) (lambda (x) (f ((lambda (x) x) x)))))
; (define one (lambda (f) (lambda (x) (f x))))

(define zero (lambda (f) (lambda (x) x)))
(define one (lambda (f) (lambda (x) (f x))))
(define two (lambda (f) (lambda (x) (f (f x)))))


; Adding f^m(x) and f^n(x) is basically composing them to get f^(m+n)(x)
(define (add m n)
  (lambda (f) (lambda (x) ((m f) ((n f) x)))))

(define (inc x)
  (+ x 1))

(display ((zero inc) 0))
(newline)

(display ((one inc) 0))
(newline)

(display ((two inc) 0))
(newline)

(define five (add (add two two) one))

(display ((five inc) 0))
(newline)

