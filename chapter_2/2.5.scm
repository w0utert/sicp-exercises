(define (cons a b)
  (* (expt 2 a) (expt 3 b)))

(define (factors n m)
  (define (factors-iter n m f) 
    (if (eq? (modulo n m) 0)
      (factors-iter (/ n m) m (+ f 1))
      f))
  (factors-iter n m 0))

(define (car z)
  (factors z 2))

(define (cdr z)
  (factors z 3))

(define ab (cons 9 12))

(display (car ab))
(newline)

(display (cdr ab))
(newline)

(define xy (cons 12 9))

(display (car xy))
(newline)

(display (cdr xy))
(newline)


