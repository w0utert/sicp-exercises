(define (make-interval a b)
  (cons a b))

(define (lower-bound x)
  (car x))

(define (upper-bound x)
  (cdr x))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (mul-interval 
    x
    (make-interval (/ 1.0 (upper-bound y))
                   (/ 1.0 (lower-bound y)))))

(define v0 (make-interval 4.8 6.3))
(define v1 (make-interval 0.3 0.9))

(define v0+v1 (add-interval v0 v1))
(define v0*v1 (mul-interval v0 v1))
(define v0/v1 (div-interval v0 v1))

(display (list (lower-bound v0+v1) (upper-bound v0+v1)))
(newline)

(display (list (lower-bound v0*v1) (upper-bound v0*v1)))
(newline)

(display (list (lower-bound v0/v1) (upper-bound v0/v1)))
(newline)
