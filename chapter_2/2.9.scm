(define (make-interval a b)
  (cons a b))

(define (lower-bound x)
  (car x))

(define (upper-bound x)
  (cdr x))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (interval-width x)
  (/ (- (upper-bound x) (lower-bound x)) 2.0))

; (interval-width v0) = (/ (- (cdr v0) (car v0)) 2.0)
; (interval-width v1) = (/ (- (cdr v1) (car v1)) 2.0)
;
; (interval-width v0+v1) 
; =
;   (/ 
;     (- 
;       (upper-bound v0+v1)
;       (lower-bound v0+v1))
;    2.0)
; = 
;   (/ 
;     (- 
;       (cdr ((+ (car v0) (car v1)) (+ (cdr v0) (cdr v1))))
;       (car ((+ (car v0) (car v1)) (+ (cdr v0) (cdr v1)))))
;    2.0)
; =
;   (/ 
;     (-
;       (+ (cdr v0) (cdr v1))
;       (+ (car v0) (car v1)))
;    2.0)
; =
;   (+
;     (/ (- (cdr v0) (car v0)) 2.0)
;     (/ (- (cdr v1) (car v1)) 2.0))
; =
;   (+
;     (interval-width v0)
;     (interval-width v1))

(define v0 (make-interval 1 3))
(define v1 (make-interval -1 5))

(define v0+v1 (add-interval v0 v1))

(display (list (interval-width v0+v1) "==" (+ (interval-width v0) (interval-width v1))))
(newline)

(display "-------------------")
(newline)

(define v2 (make-interval -2 0))

(display (list (interval-width v0) "==" (interval-width v2)))
(newline)
(display (list (interval-width (mul-interval v0 v1)) "!=" (interval-width (mul-interval v2 v1))))
(newline)

