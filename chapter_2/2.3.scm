(define (make-point x y)
  (cons x y))

(define (x-point p)
  (car p))

(define (y-point p)
  (cdr p))

(define (make-rect bottom-left width height)
  (list bottom-left width height))

(define (make-alt-rect p0 p1)
  (cons p0 p1))

(define (rect? r)
  (list? r))

(define (width-rect r)
  (if (rect? r)
    (car (cdr (cdr r)))
    (abs (- (x-point (car r)) (x-point (cdr r))))))

(define (height-rect r)
  (if (rect? r)
    (car (cdr r))
    (abs (- (y-point (car r)) (y-point (cdr r))))))

(define (perimeter-rect r)
  (+ (* (width-rect r) 2.0) (* (height-rect r) 2.0)))

(define (area-rect r)
  (* (width-rect r) (height-rect r)))

(define rect (make-rect (make-point 10 10) 20 30))

(display (perimeter-rect rect))
(newline)

(display (area-rect rect))
(newline)

(define rect-alt (make-alt-rect (make-point 10 10) (make-point 30 40)))

(display (perimeter-rect rect-alt))
(newline)

(display (area-rect rect-alt))
(newline)





