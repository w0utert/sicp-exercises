(define (cons x y)
  (lambda (m) (m x y)))

(define (car z)
  (z (lambda (p q) p)))

; (cons 10 20) => (lambda (m) (m 10 20))
; (car (cons 10 20)) 
;   => ((lambda (m) (m x y)) (lambda (p q) p)))
;   => (lambda (p q) p) 10 20)
;   => 10

(define (cdr z)
  (z (lambda (p q) q)))

(display (cdr (cons 10 20)))


