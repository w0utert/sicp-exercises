(define (cube x)
  (* x x x))

(define (inc x)
  (+ x 1))

(define (sum term a next b)
  (if (> a b)
    0
    (+ (term a)
       (sum term (next a) next b))))

(define (simpson-integral f a b n)
  (let ((h (/ (- b a) n)))
    (define (y k)
      (f (+ a (* k h))))

    (define (term-yk k)
      (cond ((or (= k 0) (= k n)) (y k))
            ((= (remainder k 2) 0) (* 2 (y k)))
            (else (* 4 (y k)))))

    (* (/ h 3) (sum term-yk 0 inc n))))

(display (simpson-integral cube 0.0 1.0 100))
(newline)

(display (simpson-integral cube 0.0 1.0 1000))
(newline)
