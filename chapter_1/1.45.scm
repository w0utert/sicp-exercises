(define tolerance 0.000000000001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2))
       tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
        next
        (try next))))
  (try first-guess))

(define (compose f g)
  (lambda (x) (f (g x))))

(define (repeated f n)
  (if (< n 2)
    f
    (compose f (repeated f (- n 1)))))

(define (average a b)
  (/ (+ a b) 2))

(define (average-damp f)
  (lambda (x) (average x (f x))))

(define (log2 x)
  (/ (log x) (log 2)))

; 4th-root (n = 2^2) => damp 2x
; 8th-root (n = 2^3) => damp 3x
; 16th-root (n = 2^4) => damp 4x
; 32th-root (n = 2^5) => damp 5x
;
; # damping => ceil(log2(n)))

(define (nth-root x n)
  (let ((damped (repeated average-damp (+ (floor (log2 n)) 1))))
    (fixed-point (damped (lambda (y) (/ x (expt y (- n 1))))) 1)))

(define n 513)
(define r (nth-root 2.0 n))

(display r)
(newline)
(display (expt r n))



