(import (srfi srfi-19))

(define (smallest-divisor n) (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b )
  (= (remainder b a) 0))

(define (square a)
  (* a a))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (current-time)))

(define (start-prime-test n start-time)
  (let ((is-prime (prime? n)))
    (when is-prime
      (report-prime n (time-difference (current-time) start-time)))
    is-prime))

(define (report-prime n elapsed-time)
  (display n)
  (display " *** ")
  (display `(,(/ (time-nanosecond elapsed-time) 1e6) "ms"))
  (newline))
  
(define (search-for-primes x max-x n)
  (if (= (remainder x 2) 0)
    (search-for-primes (+ x 1) max-x n)
    (when (and (> n 0) (< x max-x))
      (let ((is-prime (start-prime-test x (current-time))))
        (search-for-primes (+ x 2) max-x (- n (if is-prime 1 0)))))))


(search-for-primes 1000 10000 3)
(newline)

(search-for-primes 10000 100000 3)
(newline)

(search-for-primes 100000 1000000 3)
(newline)

(search-for-primes 1000000 10000000 3)
(newline)

(search-for-primes 10000000 100000000 3)
(newline)






