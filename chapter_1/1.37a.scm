(define (cont-frac n d k)
  (define (cont-frac-i n d k i)
    (if (eq? i k)
      (/ (n i) (d i))
      (/ (n i) (+ (d i) (cont-frac-i n d k (+ i 1))))))
  (cont-frac-i n d k 1))

(define (inv-phi-k k)
  (cont-frac (lambda (i) 1.0) 
             (lambda (i) 1.0) 
             k))

(define (find-k)
  (define (find-k-i i)
    (let ((inv-phi (inv-phi-k i)))
      (display (list i inv-phi))
      (newline)
      (if (< (abs (- inv-phi (/ 1.0 1.618033989))) 0.0001)
        inv-phi
        (find-k-i (+ i 1)))))
  (find-k-i 1))

(find-k)
