(define (fib n)
  (fib-iter 1 0 0 1 n))

(define (fib-iter a b p q count)
  (cond ((= count 0) b)
        ((even? count)
         (fib-iter a
                   b
                   (+ (* p p) (* q q))
                   (+ (* p q) (* q q) (* q p))
                   (/ count 2)))
        (else (fib-iter (+ (* b q) (* a q) (* a p))
                        (+ (* b p) (* a q))
                        p
                        q
                        (- count 1)))))

(display (map fib '(1 2 3 4 5 6 7 8 9 10 11)))
(newline)

; a' => bq + aq + ap 
; b' => bp + aq

; a'' => (bp + aq)*q + (bq + aq + ap)*q + (bq + aq + ap)*p
; b'' => (bp + aq)*p + (bq + aq + ap)*q

; a'' => bpq + aqq + bqq + aqq + apq + bqp + aqp + app
; b'' => bpp + aqp + bqq + aqq + apq

; a'' => b*(pq + qq + qp) + a*(pq + qq + qp) + a*(qq + pp)
; b'' => b*(pp + qq) + a*(pq + qq + qp)


