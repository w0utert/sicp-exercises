(define (double n)
  (+ n n))

(define (halve n)
  (/ n 2))

(define (mul a b)
  (define (iter a b x)
    (cond
      ((= b 0) 0)
      ((= b 1) (+ x a))
      ((even? b) (iter (double a) (halve b) x))
      (else (iter a (- b 1) (+ x a)))))
  (iter a b 0))

(display (mul 4 19)) 
(newline)
