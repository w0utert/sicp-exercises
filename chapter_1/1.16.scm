(define (even? n)
  (= (remainder n 2) 0))

(define (fast-exp-iter b n)
  (define (iter b n a)
    (cond ((= n 0) 1)
          ((= n 1) (* a b))
          ((even? n) (iter (* b b) (/ n 2) a))
          (else (iter b (- n 1) (* a b)))))
  (iter b n 1))

(display (fast-exp-iter 3 11)) ; 3^11 = 177147
(newline)
