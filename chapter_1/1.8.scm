(define (good-enough? y previous-y)
  (< (abs (/ (- previous-y y) y)) 0.001))

(define (cube x)
  (* (* x x) x))

(define (improve-y x y)
  (/ 
    (+ 
       (/ x (* y y)) 
       (* 2 y)) 
     3))

(define (cube-root-iter y previous-y x)
  (if (good-enough? y previous-y)
    y
    (cube-root-iter (improve-y x y) y x)))

(define (cube-root x)
  (cube-root-iter (/ x 2) x x))

(display (cube (cube-root 1000.0)))
(newline)

(display (cube (cube-root 3.141592657)))
(newline)

