(define (cube x)
  (* x x x))

(define (inc x)
  (+ x 1))

(define (sum-iter term a next b)
  (define (iter a result)
    (if (> a b)
      result
      (iter (next a) (+ result (term a)))))
  (iter a 0))

(display (sum-iter cube 0 inc 10))
(newline)
