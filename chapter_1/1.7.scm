(define (average x y)
  (/ (+ x y) 2))

(define (improve guess x)
  (average guess (/ x guess)))

(define (square x)
  (* x x))

(define (good-enough? guess previous-guess)
  (< (abs (/ (- previous-guess guess) guess)) 0.001))

(define (sqrt-iter guess previous-guess x)
  (if (good-enough? guess previous-guess)
      guess
      (sqrt-iter (improve guess x) guess x)))

(define (sqrt x)
  (sqrt-iter 1.0 x x))

(display (square (sqrt 9.0)))
(newline)

(display (square (sqrt 0.00002)))
(newline)

(display (square (sqrt 2.0e16)))
(newline)

