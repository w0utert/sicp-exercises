(define (cont-frac n d k)
  (define (cont-frac-i-iter n d k i a)
    (if (eq? i 0) 
      a 
     (cont-frac-i-iter n d k (- i 1) (/ (n i) (+ (d i) a)))))
  (cont-frac-i-iter n d k k 0))

(display
  (+ 2 (cont-frac 
         (lambda (i) 1.0) 
         (lambda (i) 
           (if (eq? (modulo i 3) 2) 
             (* 2 (+ (quotient i 3) 1)) 
             1)) 
         10)))
