(define (identity x)
  x)

(define (inc x)
  (+ x 1))

(define (accumulate combiner null-value term a next b)
  (define (iter x a b)
    (if (> a b)
      x
      (iter (combiner x (term a)) (next a) b)))
  (iter null-value a b))
  
(define (sum a b)
  (accumulate + 0 identity a inc b))

(define (product a b)
  (accumulate * 1 identity a inc b))

(display (sum 1 10))
(newline)

(display (product 1 10))
(newline)
