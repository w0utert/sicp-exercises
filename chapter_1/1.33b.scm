(define (identity x)
  x)

(define (inc x)
  (+ x 1))

(define (filtered-accumulate combiner null-value term a next b filter?)
  (define (iter x a b)
    (if (> a b)
      x
      (if (filter? a)
        (iter (combiner x (term a)) (next a) b)
        (iter (combiner x null-value) (next a) b))))
  (iter null-value a b))
  
(define (sum-relative-primes a b)
  (define (relative-prime? x)
    (eq? (gcd x b) 1))
  (filtered-accumulate + 0 identity a inc b relative-prime?))

(display (sum-relative-primes 1 10))
(newline)
