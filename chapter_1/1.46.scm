(define (average x y)
  (/ (+ x y) 2))

(define (square x)
  (* x x))

(define (iterative-improve good-enough? improve)
  (lambda (guess)
    (define (improve-iter g)
      (if (good-enough? g)
        g
        (improve-iter (improve g))))
    (improve-iter guess)))

(define (sqrt x)
  ((iterative-improve 
     (lambda (g) (< (abs (- (square g) x)) 0.001)) 
     (lambda (g) (average g (/ x g)))) 
   x))

(define (fixed-point f first-guess)
  ((iterative-improve
     (lambda (g) (< (abs (- (f g) g)) 0.001))
     (lambda (g) (f g)))
   first-guess))

(display (square (sqrt 10.0)))
(newline)

(display (fixed-point (lambda (x) (+ 1 (/ 1 x))) 2.0))
(newline)


