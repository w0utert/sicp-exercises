(define (identity x)
  x)

(define (inc x)
  (+ x 1))

(define (accumulate combiner null-value term a next b)
  (if (> a b)
    null-value
    (combiner a (accumulate combiner null-value term (next a) next b))))

(define (sum a b)
  (accumulate + 0 identity a inc b))

(define (product a b)
  (accumulate * 1 identity a inc b))

(display (sum 1 10))
(newline)

(display (product 1 10))
(newline)
