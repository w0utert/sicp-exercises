(import (srfi srfi-19))

(define NUM-TESTS 20)

(define (square a)
  (* a a))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (if (and
               (= exp 2)
               (not (= base 1))
               (not (= base (- m 1)))
               (= (remainder (square base) m) 1))
           0
           (remainder 
             (square (expmod base (/ exp 2) m))
             m)))
        (else
          (remainder
            (* base (expmod base (- exp 1) m))
            m))))

(define (miller-rabin-test n)
  (define (try-it a)
    (let ((x (expmod a (- n 1) n))) 
      (if (= x 0)
        #f
        (= x 1))))
  (try-it (+ 1 (random (- n 1)))))

(define (run-prime-test n times)
  (cond ((= times 0) #t) 
        ((miller-rabin-test n) (run-prime-test n (- times 1)))
        (else #f)))

(define (prime? n times)
  (if (or (= n 1) (= (remainder n 2) 0))
    #f
    (run-prime-test n times)))

(display "Carmichael numbers\n")
(display (prime? 2465 NUM-TESTS))
(newline)
(display (prime? 2821 NUM-TESTS))
(newline)
(display (prime? 6601 NUM-TESTS))
(newline)

(display "Prime numbers\n")
(display (prime? 10007 NUM-TESTS))
(newline)
(display (prime? 1000037 NUM-TESTS))
(newline)
(display (prime? 10000079 NUM-TESTS))
(newline)

(display "Non-prime, non-charmichael numbers\n")
(display (prime? 1 NUM-TESTS))
(newline)
(display (prime? 2 NUM-TESTS))
(newline)
(display (prime? 999999 NUM-TESTS))
(newline)
(display (prime? 10298304 NUM-TESTS))
(newline)








