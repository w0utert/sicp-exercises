(define (p n m)
  (cond ((eq? n 0) 1)
        ((or (eq? m 0) (eq? m n)) 1)
        ((or (< m 0) (>= m n)) 0)
        (else (+ 
                (p (- n 1) (- m 1)) 
                (p (- n 1) m)))))

(define (pascal-row n)
  (define (rest cur m)
    (if (> m n)
      cur
      (rest (append cur (list (p n m))) (+ m 1))))
  (rest '() 0))

(define (pascal rows)
  (define (iter n)
    (when (< n rows)
      (display (pascal-row n))
      (newline)
      (iter (+ n 1))))
  (iter 0))

(pascal 5)
