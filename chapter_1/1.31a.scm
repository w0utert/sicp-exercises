(define (inc a)
  (+ a 1))

(define (identity a)
  a)

(define (product term a next b)
  (if (> a b)
    1
    (* (term a) (product term (next a) next b))))

(define (factorial x)
  (product identity 1 inc x))

(define (approximate-pi n)
  (define (step-2 x)
    (* 2 (+ 1 (quotient x 2))))

  (define (pi-term x)
    (/ 
      (step-2 x)
      (+ (step-2 (- x 1)) 1)))

  (* 4 (product pi-term 1 inc n)))

(display (factorial 9))
(newline) 

(display (exact->inexact (approximate-pi 50)))
(newline)

