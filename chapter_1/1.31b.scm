(define (inc a)
  (+ a 1))

(define (identity a)
  a)

(define (product-iter term a next b)
  (define (iter a result)
    (if (> a b)
      result
      (iter (next a) (* (term a) result))))
  (iter a 1))

(define (factorial x)
  (product-iter identity 1 inc x))

(define (approximate-pi n)
  (define (step-2 x)
    (* 2 (+ 1 (quotient x 2))))

  (define (pi-term x)
    (/ 
      (step-2 x)
      (+ (step-2 (- x 1)) 1)))

  (* 4 (product-iter pi-term 1 inc n)))

(display (factorial 9))
(newline) 

(display (exact->inexact (approximate-pi 50)))
(newline)

