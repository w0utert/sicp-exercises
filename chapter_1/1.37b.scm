(define (cont-frac n d k)
  (define (cont-frac-i-iter n d k i a)
    (if (eq? i 0) 
      a 
     (cont-frac-i-iter n d k (- i 1) (/ (n i) (+ (d i) a)))))
  (cont-frac-i-iter n d k k 0))

(define (inv-phi-k k)
  (cont-frac (lambda (i) 1.0) 
             (lambda (i) 1.0) 
             k))

(define (find-k)
  (define (find-k-i i)
    (let ((inv-phi (inv-phi-k i)))
      (display (list i inv-phi))
      (newline)
      (if (< (abs (- inv-phi (/ 1.0 1.618033989))) 0.0001)
        inv-phi
        (find-k-i (+ i 1)))))
  (find-k-i 1))

(find-k)
