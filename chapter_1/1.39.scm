(define (cont-frac n d k)
  (define (cont-frac-i-iter n d k i a)
    (if (eq? i 0) 
      a 
     (cont-frac-i-iter n d k (- i 1) (/ (n i) (+ (d i) a)))))
  (cont-frac-i-iter n d k k 0))

(define (tan-cf x k)
  (cont-frac 
    (lambda (i) (if (eq? i 1) x (* -1.0 (* x x))))
    (lambda (i) (- (* i 2) 1))
    k))

(display (tan-cf 1.0 10))
(newline)
(display (tan-cf 2.0 10))
(newline)
(display (tan-cf (acos -1.0) 20))
