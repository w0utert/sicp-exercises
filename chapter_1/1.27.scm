(define (square a)
  (* a a))

(define (divides? a b )
  (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (smallest-divisor n) (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder 
           (square (expmod base (/ exp 2) m))
           m))
        (else
          (remainder
            (* base (expmod base (- exp 1) m))
            m))))

(define (carmichael? n)
  (define (iter a n)
    (cond
      ((= a n) (not (prime? n)))
      ((not (= (expmod a n n) a)) #f)
      (else (iter (+ a 1) n))))
  (iter 1 n))

; Carmichael numbers
(display (carmichael? 561)) 
(newline)

(display (carmichael? 1105))
(newline)

(display (carmichael? 1729))
(newline)

(display (carmichael? 2465))
(newline)

(display (carmichael? 2821))
(newline)

(display (carmichael? 6601))
(newline)

; Prime
(display (carmichael? 10007))
(newline)

; Not prime, not carmichael
(display (carmichael? 10298304))
(newline)

