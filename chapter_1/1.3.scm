(define (f a b c)
  (let ((min (if (< a b) (if (< a c) a c) (if (< b c) b c))))
    (- 
      (+ (* a a) (* b b) (* c c)) 
      (* min min))))

(display (f 8 4 1))
(newline)
