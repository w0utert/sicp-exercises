(define (f-recursive n)
  (cond ((< n 3) n)
        (else (+ (f-recursive (- n 1)) 
                 (* 2 (f-recursive (- n 2))) 
                 (* 3 (f-recursive (- n 3)))))))

(define (f-iterative n)
  (define (iter fn-3 fn-2 fn-1 count)
    (cond ((= count 0) fn-3)
          ((= count 1) fn-2)
          ((= count 2) fn-1)
          (else (iter fn-2 fn-1 (+ fn-1 (* 2 fn-2) (* 3 fn-3)) (- count 1)))))
  (iter 0 1 2 n))


(define (eval-n f n)
  (define (iter i)
    (when (< i n)
      (display (f i))
      (newline)
      (iter (+ i 1))))
  (iter 0))

(display "recursive\n")
(eval-n f-recursive 10)

(newline)

(display "iterative\n")
(eval-n f-iterative 10)

