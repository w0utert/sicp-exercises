(import (srfi srfi-19))

(define NUM-TESTS 20)

(define (square a)
  (* a a))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder 
           (square (expmod base (/ exp 2) m))
           m))
        (else
          (remainder
            (* base (expmod base (- exp 1) m))
            m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) #t)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else #f)))

(define (start-prime-test n num-tests start-time)
  (let ((is-prime (fast-prime? n num-tests)))
    (when is-prime
      (report-prime n (time-difference (current-time) start-time)))
    is-prime))

(define (report-prime n elapsed-time)
  (display n)
  (display " *** ")
  (display `(,(/ (time-nanosecond elapsed-time) 1e6) "ms"))
  (newline))
  
(define (search-for-primes x max-x n)
  (if (= (remainder x 2) 0)
    (search-for-primes (+ x 1) max-x n)
    (when (and (> n 0) (< x max-x))
      (let ((is-prime (start-prime-test x NUM-TESTS (current-time))))
        (search-for-primes (+ x 2) max-x (- n (if is-prime 1 0)))))))

(search-for-primes 1000 10000 3)
(newline)

(search-for-primes 10000 100000 3)
(newline)

(search-for-primes 100000 1000000 3)
(newline)

(search-for-primes 1000000 10000000 3)
(newline)

(search-for-primes 10000000 100000000 3)
(newline)






