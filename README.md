# SICP exercises

## Description
Scheme code for exercises from the book [Structure and Interpretation of Computer Programs](https://mitpress.mit.edu/9780262510875/structure-and-interpretation-of-computer-programs/) by Abelson, Sussman & Sussman.
